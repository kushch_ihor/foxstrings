﻿using MaxStringValue.Helpers;
using System;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("MaxStringValue.Tests")]
namespace MaxStringValue
{

    class Program
    {
        static void Main(string[] args)
        {
            var path = args.FirstOrDefault() ?? string.Empty;
            if (string.IsNullOrWhiteSpace(path))
            {
                Console.WriteLine("Please enter the file path...");
                do
                {
                    path = Console.ReadLine();
                } while (string.Empty.Equals(path));
            }
            var processor = ProcessorResolver.GetProcessor();
            processor.Process(path);
        } 
    }
}

﻿using MaxStringValue.Components;

namespace MaxStringValue.Contracts
{
    interface IProcessor
    {
        public void Process(string path);
        public StringInfo StringInfo { get; }
    }
}

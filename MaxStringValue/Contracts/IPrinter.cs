﻿using System.Collections.Generic;

namespace MaxStringValue.Contracts
{
    internal interface IPrinter
    {
        void PrintResult(int maxString, IEnumerable<int> corruptedValues);
    }
}
﻿using MaxStringValue.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MaxStringValue.Components.Printers
{
    class ConsolePrinter : IPrinter
    {
        public void PrintResult(int maxString, IEnumerable<int> corruptedValues)
        {
            PrinMaxValue(maxString);
            PrintInvalidlines(corruptedValues);
        }
        private void PrintInvalidlines(IEnumerable<int> lines)
        {
            if (lines.Any())
                Console.WriteLine($"Following rows were corrupted by the invalid characters:");
            foreach (var line in lines)
                Console.WriteLine($"Line number -> {line}");
        }

        private void PrinMaxValue(int maxResult)
        {
            if (maxResult < 0) return;

            Console.WriteLine($"Max values row number -> {maxResult}");
            Console.WriteLine($"-------------------------------------------------");
        }
    }
}

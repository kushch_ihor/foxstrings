﻿using MaxStringValue.Contracts;
using MaxStringValue.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace MaxStringValue.Components.Printers
{
    class FilePrinter : IPrinter
    {
        private string fileToWrite;
        public FilePrinter() { }
        public FilePrinter(string fileToWrite)
        {
            this.fileToWrite = fileToWrite;
        }

        public void PrintResult(int maxString, IEnumerable<int> corruptedValues)
        {
            PrintResult(fileToWrite, maxString, corruptedValues);
        }

        public void PrintResult(string path, int maxString, IEnumerable<int> corruptedValues)
        {
            var result = new[] { $"Max Value String Number -> {maxString}" };

            if (corruptedValues != null)
                result = result.Concat(corruptedValues.Select(o => $"Incorrect string number -> {o}")).ToArray();
            
            FileHelper.WriteFile(path, result);
        }
    }
}

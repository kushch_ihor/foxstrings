﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MaxStringValue.Components
{
    class StringInfo
    {
        private readonly string[] values;
        public Dictionary<int, string> InvalidItems { get; } = new Dictionary<int, string>();

        public int MaxResult { get; private set; } = -1;

        private double? maxSum = null;

        public StringInfo(string[] baseCollection)
        {
            values = baseCollection;
            ProcessCollection();
        }

        private void ProcessCollection()
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (string.IsNullOrWhiteSpace(values[i]))
                    continue;

                var splitted = values[i].Split(',');
                if (splitted.All(o => double.TryParse(o, out var _)))
                    AddMaxValue(i, splitted);
                else
                    AddCorrupted(i, values[i]);
            }
        }

        private void AddCorrupted(int index, string value)
        {
            var rowNumber = ++index;
            InvalidItems.Add(rowNumber, value);
        }

        private void AddMaxValue(int index, string[] splitted)
        {
            if (!splitted.Any()) return;
            var rowNumber = ++index;

            var splittedSum = splitted.Sum(o => double.Parse(o));
            if (MaxResult == -1)
                MaxResult = rowNumber;
            MaxResult = splittedSum > maxSum ? rowNumber : MaxResult;

            if (!maxSum.HasValue)
                maxSum = splittedSum;
            maxSum = Math.Max(maxSum.Value, splittedSum);
        }
    }
}

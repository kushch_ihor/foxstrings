﻿using MaxStringValue.Contracts;
using MaxStringValue.Helpers;
using System.Linq;

namespace MaxStringValue.Components.Processors
{
    class StringProcessor : IProcessor
    {
        public StringInfo StringInfo { get; private set; }
        private readonly IPrinter printer;
        public StringProcessor(IPrinter printer)
        {
            this.printer = printer;
        }
        public void Process(string path)
        {
            var lines = FileHelper.ReadFile(path);
            StringInfo = new StringInfo(lines);
            printer.PrintResult(StringInfo.MaxResult, StringInfo.InvalidItems.Select(o => o.Key));
        }
    }
}

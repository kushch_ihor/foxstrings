﻿using System;
using System.IO;

namespace MaxStringValue.Helpers
{
    public static class FileHelper
    {
        public static string[] ReadFile(string path)
        {
            CheckFile(path);
            return File.ReadAllLines(path);
        }

        public static void WriteFile(string path, string[] content) 
        {
            CheckFile(path);
            File.AppendAllLines(path, content);
        }

        public static void CheckFile(string path)
        {
            if (!File.Exists(path)) throw new ArgumentException("Incorrect file path");
        }
    }
}

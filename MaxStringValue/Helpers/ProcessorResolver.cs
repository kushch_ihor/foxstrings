﻿using MaxStringValue.Components.Printers;
using MaxStringValue.Components.Processors;
using MaxStringValue.Contracts;

namespace MaxStringValue.Helpers
{
    static class ProcessorResolver
    {
        public static IProcessor GetProcessor()
        {
            var printer = GetPrinter();
            return GetProcesor(printer);
        }

        private static IProcessor GetProcesor(IPrinter printer)
        {
            switch (Constants.Constants.PROCESSOR_TYPE.ToLower())
            {
                case "text": return new TextProcessor();
                case "string": return new StringProcessor(printer);
                default: return new StringProcessor(printer);
            }
        }

        private static IPrinter GetPrinter()
        {
            switch (Constants.Constants.PRINTER_TYPE.ToLower())
            {
                case "console": return new ConsolePrinter();
                case "file": return new FilePrinter();
                default: return new ConsolePrinter();
            }
        }
    }
}

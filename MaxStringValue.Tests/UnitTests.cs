using NUnit.Framework;
using System.Reflection;
using System.IO;
using System;
using System.Linq;
using System.Collections.Generic;
using MaxStringValue.Components.Printers;
using MaxStringValue.Components;
using MaxStringValue.Helpers;

namespace MaxStringValue.Tests
{
    public class Tests
    {
        private string testFolder = "TestResults";
        private string relativePath => Path.GetDirectoryName(Assembly.GetAssembly(typeof(Tests)).Location);

        private string testContent = "12.46897, 4.4512";

        private string fileName = "testExist.txt";

        private FilePrinter helper => new FilePrinter();

        [SetUp]
        public void Setup()
        {
            var dirPath = Path.Combine(relativePath, testFolder);
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);
            if (!File.Exists(Path.Combine(dirPath, fileName)))
                File.WriteAllLines(Path.Combine(dirPath, fileName), new[] { testContent });
        }

        [Test]
        public void ReadFileTest()
        {
            var path = Path.Combine(relativePath, testFolder, fileName);
            var actual = FileHelper.ReadFile(path).First();
            Assert.AreEqual(testContent, actual);
        }

        [Test]
        public void ReadNonExistFileTest()
        {
            Assert.Throws(typeof(ArgumentException), () => FileHelper.ReadFile(relativePath));
        }

        [Test]
        public void WriteToNonExixsFileTest()
        {
            Assert.Throws(typeof(ArgumentException), () => helper.PrintResult(relativePath, default, Enumerable.Empty<int>()));
        }

        [Test]
        [TestCase(new string[0], -1)]
        [TestCase(new string[] { "", "0" }, 2)]
        [TestCase(new string[] { "20,266,455", "0.5,56" }, 1)]
        [TestCase(new string[] { "", "0.5,56" }, 2)]
        [TestCase(new string[] { "-123" }, 1)]
        public void MaxValueRowTest(string[] values, int rowIndex)
        {
            var stringHelper = new StringInfo(values);
            Assert.AreEqual(rowIndex, stringHelper.MaxResult);
        }

        [Test]
        [TestCase(new string[0], new int[0])]
        [TestCase(new string[] { "20,266,455", "0.5,56" }, new int[0])]
        [TestCase(new string[] { "lalal, 123, 546", "lllalalal, 5.255", "aaa" }, new[] { 1, 2, 3 })]
        [TestCase(new string[] { "123, 546", "5.255", "aaa" }, new[] { 3 })]
        [TestCase(new string[] { "-123, -546", "-5.255", "aaa" }, new[] { 3 })]

        public void InvalidRowtest(string[] values, IEnumerable<int> expexted)
        {
            var stringHelper = new StringInfo(values);
            CollectionAssert.AreEqual(expexted, stringHelper.InvalidItems.Keys.ToArray());
        }
    }
}